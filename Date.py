from bottle import run, route 
import subprocess
## Setting Route
@route('/date')
def index():
    output = subprocess.check_output('date')
    return output

if __name__ == '__main__':
    run(host='localhost', port=8080, debug=True)
